import urllib3
import os
import csv
from datetime import datetime, timedelta
import re
from datetime import datetime
import json
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
import psycopg2
import pandas as pd
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.options import Options
from selenium import webdriver


def Load_Csv(period, words, driver, path):
    file = os.listdir(path)
    for f in file:
        os.remove(path + f)

        ## browser = webdriver.Firefox(firefox_profile=fp)
    driver.get('https://trends.google.ru/trends/?geo=RU')
    http = 'https://trends.google.ru/trends/explore?date=today '
    if period == '3-m' or period == '1-m':
        url = 'https://trends.google.ru/trends/explore?date=today ' + period + '&q='
    elif period == '12-m':
        url = 'https://trends.google.ru/trends/explore?q='
    elif period == '7-d':
        url = 'https://trends.google.ru/trends/explore?date=now ' + period + '&q='

    url = url + words

    driver.get(url)
    elem = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH,
                                                                           '/html/body/div[2]/div[2]/div/md-content/div/div/div[1]/trends-widget/ng-include/widget/div/div/div/widget-actions/div/button[1]')))
    elem.click()


def Load_CsvDay(period, words, driver, path):
    file = os.listdir(path)
    for f in file:
        os.remove(path + f)

        ## browser = webdriver.Firefox(firefox_profile=fp)
    driver.get('https://trends.google.ru/trends/?geo=RU')
    http = 'https://trends.google.ru/trends/explore?date=today '
    url = 'https://trends.google.ru/trends/explore?date=now 7-d&q='
    url = url + words

    driver.get(url)
    elem = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH,
                                                                           '/html/body/div[2]/div[2]/div/md-content/div/div/div[1]/trends-widget/ng-include/widget/div/div/div/widget-actions/div/button[1]')))
    elem.click()


def Settings():
    file = '/www/telegram/settings.txt'
    with open(file, 'r', encoding='utf8') as f:
        data = f.read()
        data = data.split('\n')
        setting = {}
        setting['host'] = data[0].replace('host:', '').replace(' ', '')
        setting['login'] = data[1].replace('login:', '').replace(' ', '')
        setting['password'] = data[2].replace('password:', '').replace(' ', '')
        return setting


def Get_words(conn, date_start):
    Word = []
    words = []
    sql = '''SELECT title, date_update FROM coinmarketcap.coin where date_update>'date_start' order by market_cap_usd desc limit 200;'''
    sql=sql.replace('date_start',date_start)
    try:
        cur = conn.cursor()
        # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
        cur.execute(sql)
        data = cur.fetchall()
    except psycopg2.Error as err:
        print("Query error: {}".format(err))
    for js in data:
        words.append(js[0].lower())
    Main_w = 'bitcoin'
    index = 0
    temp_w = []
    for i in range(0, len(words)):
        temp_w.append(words[i])
        index += 1
        if index == 4:
            words_str = Main_w + ',' + temp_w[0] + ',' + temp_w[1] + ',' + temp_w[2] + ',' + temp_w[3]
            Word.append(words_str)
            index = 0
            temp_w = []
    return Word


def Get_symbol(conn):
    Word = []
    words = []
    sql = '''SELECT symbol FROM coinmarketcap.coin where title not in ('Bitcoin')group by symbol limit 1000;'''
    try:
        cur = conn.cursor()
        # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
        cur.execute(sql)
        data = cur.fetchall()
    except psycopg2.Error as err:
        print("Query error: {}".format(err))
    for js in data:
        words.append(js[0].lower())
    Main_w = 'bitcoin'
    index = 0
    temp_w = []
    for i in range(1, len(words)):
        temp_w.append(words[i])
        index += 1
        if index == 4:
            words_str = Main_w + ',' + temp_w[0] + ',' + temp_w[1] + ',' + temp_w[2] + ',' + temp_w[3]
            Word.append(words_str)
            index = 0
            temp_w = []
    Word.append('bitcoin,ppt')
    return Word


def Get_wordsTrends(conn, name):
    Word = []
    words = []
    if name == 'title':
        sql = '''SELECT words FROM google.trends group by words;'''
    else:
        sql = '''SELECT words FROM google.trends_symbol group by words;'''
    try:
        cur = conn.cursor()
        # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
        cur.execute(sql)
        data = cur.fetchall()
    except psycopg2.Error as err:
        print("Query error: {}".format(err))
    for js in data:
        words.append(js[0].lower())

    return words


def Get_Csv(path):
    count = 0
    Data = []
    file = os.listdir(path)
    for f in file:
        if re.search('multi', f):
            with open(path + f, 'r', encoding='utf8') as file_data:
                file_data = file_data.read()
            file_data_list = file_data.split('\n')
            file_Reader = csv.reader(file_data_list)
            for fields in file_Reader:
                if count == 2:
                    count += 1
                    column = []
                    for ff in fields:
                        if re.search('Week', ff):
                            column.append('date')
                        else:
                            column.append(ff.replace(': (All word)', ''))

                elif count < 3 and count != 2:
                    count += 1
                    continue
                else:
                    Data.append(fields)

    os.remove(path + f)
    return Data


def average(day, name):
    Index = []
    Data = []
    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))

    Words = Get_wordsTrends(conn, name)
    if name == 'symbol':
        sql = """SELECT date_period
     FROM google.trends
     order by date_period desc limit 1;"""
    else:
        sql = """SELECT date_period
             FROM google.trends_symbol
             order by date_period desc limit 1;"""

    try:
        cur = conn.cursor()
        # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
        cur.execute(sql)
        data = cur.fetchall()
    except psycopg2.Error as err:
        print("Query error: {}".format(err))

    date_start = datetime.strptime(data[0][0], '%Y-%m-%d %H:%M:%S')
    date_end = date_start - timedelta(days=day)
    for w in Words:
        if name == 'title':
            sql = '''select words, value1,value2,value3,value4,value5
from google.trends
where date_period<'date_start' and date_period>'date_end' and words='words_temp';'''
        else:
            sql = '''select words, value1,value2,value3,value4,value5
            from google.trends_symbol
            where date_period<'date_start' and date_period>'date_end' and words='words_temp';'''
        sql = sql.replace('date_start', str(date_start)).replace('date_end', str(date_end)).replace('words_temp', w)
        try:
            cur = conn.cursor()
            # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
            cur.execute(sql)
            data = cur.fetchall()
        except psycopg2.Error as err:
            print("Query error: {}".format(err))
        Sum = [0, 0, 0, 0]
        for d in data:
            for i in range(0, 4):
                Sum[i] += d[i + 2]
        for i in range(0, 4):
            Sum[i] = Sum[i] / len(data)
        for s in Sum:
            Data.append(s)

        column = w.split(',')
        if name == 'title':
            column.remove('bitcoin')
        else:
            column.remove('btc')
        for c in column:
            Index.append(c)
    df = pd.DataFrame(Index, index=Data)
    df.sort_index(inplace=True)

    return df


def average_word(name):
    Index = []
    Data = []
    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))

    Words = Get_wordsTrends(conn, name)
    sql = """SELECT date_period
         FROM google.trends
         order by date_period desc limit 1;"""

    try:
        cur = conn.cursor()
        # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
        cur.execute(sql)
        data = cur.fetchall()
    except psycopg2.Error as err:
        print("Query error: {}".format(err))

    date_start = datetime.now()
    date_end = date_start - timedelta(days=30)
    for w in Words:
        sql = '''SELECT word, direct, "relative", concerning_word, processing_dttm
FROM google.trends_word
where word='words_temp' and processing_dttm>'date_start' and processing_dttm<'date_end';'''
        sql = sql.replace('date_start', str(date_start)).replace('date_end', str(date_end)).replace('words_temp', w)
        try:
            cur = conn.cursor()
            # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
            cur.execute(sql)
            data = cur.fetchall()
        except psycopg2.Error as err:
            print("Query error: {}".format(err))
        Sum = [0, 0, 0, 0]
        for d in data:
            for i in range(0, 4):
                Sum[i] += d[i + 2]
        for i in range(0, 4):
            Sum[i] = Sum[i] / len(data)
        for s in Sum:
            Data.append(s)

        column = w.split(',')
        column.remove('Bitcoin')
        for c in column:
            Index.append(c)
    df = pd.DataFrame(Index, index=Data)
    df.sort_index(inplace=True)

    return df


def Settings():
    file = '/www/telegram/settings.txt'
    with open(file, 'r', encoding='utf8') as f:
        data = f.read()
        data = data.split('\n')
        setting = {}
        setting['host'] = data[0].replace('host:', '').replace(' ', '')
        setting['login'] = data[1].replace('login:', '').replace(' ', '')
        setting['password'] = data[2].replace('password:', '').replace(' ', '')
        return setting


def Load(driver):
    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))

    period = '3-m'
    Words = Get_words(conn)
    if period == '3-m':
        period = '1-d'
    count = 0
    for w in Words:
        if count < 150:
            count += 1
            continue
        else:
            w = w.lower()
            Load_Csv('3-m', w, driver, '/www/telegram/File/')
            Data = Get_Csv('/www/telegram/File/')
            print(w)
            w = w.split(',')
            w.remove('bitcoin')
            for j in range(0, 4):

                sql = '''SELECT  processed_dttm FROM google.trends_s where word='test' order by processed_dttm desc;'''
                sql = sql.replace('test', w[j])
                try:
                    cur = conn.cursor()

                    cur.execute(sql)
                    data = cur.fetchall()
                except psycopg2.Error as err:
                    print("Query error: {}".format(err))
                if len(data) == 0:
                    Date_Start = []
                else:
                    Date_Start = []
                    for d in data:
                        Date_Start.append(d[0])

                for i in range(0, len(Data) - 2):
                    if len(Data[i]) == 0:
                        break
                    try:
                        data_end = datetime.strptime(Data[i][0] + ' 00:00', '%Y-%m-%d %H:%M')
                    except:
                        continue

                    if data_end not in Date_Start:

                        sql = """INSERT INTO google.trends_s(word, period, value, processed_dttm) VALUES ("""
                        if re.search('<', Data[i][j + 2]):
                            Data[i][j + 2] = 1

                        try:
                            sql = sql + '\'' + w[j] + '\',' + '\'' + period + '\',' + str(Data[i][j + 2]) + ',\'' + str(
                                data_end) + '\');'
                        except:
                            break
                        try:
                            cur = conn.cursor()

                            cur.execute(sql)
                            conn.commit()
                        except psycopg2.Error as err:
                            print("Query error: {}".format(err))
    return 1


def Load_Symbol(driver):
    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))

    period = '3-m'
    Words = Get_symbol(conn)
    if period == '3-m':
        period = '1-d'
    for w in Words:
        flag = True
        sql = """SELECT words
 FROM google.trends_symbol
 group by words;"""
        try:
            cur = conn.cursor()
            # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
            cur.execute(sql)
            data = cur.fetchall()
        except psycopg2.Error as err:
            print("Query error: {}".format(err))
        for ww in data:
            if w == ww[0]:
                flag = False
                break
        if flag == True:
            w = w.lower()
            Load_Csv('3-m', w, driver, '/www/telegram/File1/')
            Data = Get_Csv('/www/telegram/File1/')
            print(w)
            for i in range(0, len(Data) - 2):

                if len(Data[i]) == 0:
                    break
                try:
                    data_end = datetime.strptime(Data[i][0] + ' 00:00', '%Y-%m-%d %H:%M')
                except:
                    continue
                sql = """INSERT INTO google.trends_symbol(words, period, date_period, value1, value2, value3, value4, value5)VALUES("""
                if re.search('<', Data[i][1]):
                    Data[i][1] = 1
                if re.search('<', Data[i][2]):
                    Data[i][2] = 1
                if re.search('<', Data[i][3]):
                    Data[i][3] = 1
                if re.search('<', Data[i][4]):
                    Data[i][4] = 1
                if re.search('<', Data[i][5]):
                    Data[i][5] = 1
                try:
                    sql = sql + '\'' + w + '\',' + '\'' + period + '\',' + '\'' + str(data_end) + '\',' + str(
                        Data[i][1]) + ',' + str(Data[i][2]) + ',' + str(Data[i][3]) + ',' + str(Data[i][4]) + ',' + str(
                        Data[i][5]) + ');'
                except:
                    break
                try:
                    cur = conn.cursor()
                    # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
                    cur.execute(sql)
                    conn.commit()
                except psycopg2.Error as err:
                    print("Query error: {}".format(err))
    return 1


def Load_data(symbol):
    with open('/www/telegram/Proxy.txt', 'r', encoding='utf8') as file:
        proxy = file.read()
    index = 0

    count = 0

    while True:

        ## try:
        if symbol == 'symbol':
            path = '/www/telegram/File1/'
            driver = Driver(index, path)
            if Load_Symbol(driver) == 1:
                break
        elif symbol == 'title':
            path = '/www/telegram/File/'
            driver = Driver(index, path)
            if Load(driver) == 1:
                break
                ##  except:
        driver.quit()
        index += 1
        driver = Driver(index)

    driver.quit()


def Load_Day():
    with open('/www/telegram/Proxy.txt', 'r', encoding='utf8') as file:
        proxy = file.read()
    i = 0
    while True:
        try:
            if Get_day_data(proxy[i]):
                break
        except:

            i += 1


def Get_day_data(proxy, path):
    with open('/www/telegram/Proxy.txt', 'r', encoding='utf8') as file:
        proxy = file.read()
    index = 0
    fp = webdriver.FirefoxProfile()

    fp.set_preference("browser.download.folderList", 2)

    fp.set_preference("browser.download.manager.showWhenStarting", False)
    fp.set_preference("browser.download.defaultFolder", path)
    fp.set_preference("browser.download.dir", path)
    fp.set_preference("browser.helperApps.neverAsk.saveToDisk",
                      "application/vnd.hzn-3d-crossword;video/3gpp;video/3gpp2;application/vnd.mseq;application/vnd.3m.post-it-notes;application/vnd.3gpp.pic-bw-large;application/vnd.3gpp.pic-bw-small;application/vnd.3gpp.pic-bw-var;application/vnd.3gp2.tcap;application/x-7z-compressed;application/x-abiword;application/x-ace-compressed;application/vnd.americandynamics.acc;application/vnd.acucobol;application/vnd.acucorp;audio/adpcm;application/x-authorware-bin;application/x-athorware-map;application/x-authorware-seg;application/vnd.adobe.air-application-installer-package+zip;application/x-shockwave-flash;application/vnd.adobe.fxp;application/pdf;application/vnd.cups-ppd;application/x-director;applicaion/vnd.adobe.xdp+xml;application/vnd.adobe.xfdf;audio/x-aac;application/vnd.ahead.space;application/vnd.airzip.filesecure.azf;application/vnd.airzip.filesecure.azs;application/vnd.amazon.ebook;application/vnd.amiga.ami;applicatin/andrew-inset;application/vnd.android.package-archive;application/vnd.anser-web-certificate-issue-initiation;application/vnd.anser-web-funds-transfer-initiation;application/vnd.antix.game-component;application/vnd.apple.installe+xml;application/applixware;application/vnd.hhe.lesson-player;application/vnd.aristanetworks.swi;text/x-asm;application/atomcat+xml;application/atomsvc+xml;application/atom+xml;application/pkix-attr-cert;audio/x-aiff;video/x-msvieo;application/vnd.audiograph;image/vnd.dxf;model/vnd.dwf;text/plain-bas;application/x-bcpio;application/octet-stream;image/bmp;application/x-bittorrent;application/vnd.rim.cod;application/vnd.blueice.multipass;application/vnd.bm;application/x-sh;image/prs.btif;application/vnd.businessobjects;application/x-bzip;application/x-bzip2;application/x-csh;text/x-c;application/vnd.chemdraw+xml;text/css;chemical/x-cdx;chemical/x-cml;chemical/x-csml;application/vn.contact.cmsg;application/vnd.claymore;application/vnd.clonk.c4group;image/vnd.dvb.subtitle;application/cdmi-capability;application/cdmi-container;application/cdmi-domain;application/cdmi-object;application/cdmi-queue;applicationvnd.cluetrust.cartomobile-config;application/vnd.cluetrust.cartomobile-config-pkg;image/x-cmu-raster;model/vnd.collada+xml;text/csv;application/mac-compactpro;application/vnd.wap.wmlc;image/cgm;x-conference/x-cooltalk;image/x-cmx;application/vnd.xara;application/vnd.cosmocaller;application/x-cpio;application/vnd.crick.clicker;application/vnd.crick.clicker.keyboard;application/vnd.crick.clicker.palette;application/vnd.crick.clicker.template;application/vn.crick.clicker.wordbank;application/vnd.criticaltools.wbs+xml;application/vnd.rig.cryptonote;chemical/x-cif;chemical/x-cmdf;application/cu-seeme;application/prs.cww;text/vnd.curl;text/vnd.curl.dcurl;text/vnd.curl.mcurl;text/vnd.crl.scurl;application/vnd.curl.car;application/vnd.curl.pcurl;application/vnd.yellowriver-custom-menu;application/dssc+der;application/dssc+xml;application/x-debian-package;audio/vnd.dece.audio;image/vnd.dece.graphic;video/vnd.dec.hd;video/vnd.dece.mobile;video/vnd.uvvu.mp4;video/vnd.dece.pd;video/vnd.dece.sd;video/vnd.dece.video;application/x-dvi;application/vnd.fdsn.seed;application/x-dtbook+xml;application/x-dtbresource+xml;application/vnd.dvb.ait;applcation/vnd.dvb.service;audio/vnd.digital-winds;image/vnd.djvu;application/xml-dtd;application/vnd.dolby.mlp;application/x-doom;application/vnd.dpgraph;audio/vnd.dra;application/vnd.dreamfactory;audio/vnd.dts;audio/vnd.dts.hd;imag/vnd.dwg;application/vnd.dynageo;application/ecmascript;application/vnd.ecowin.chart;image/vnd.fujixerox.edmics-mmr;image/vnd.fujixerox.edmics-rlc;application/exi;application/vnd.proteus.magazine;application/epub+zip;message/rfc82;application/vnd.enliven;application/vnd.is-xpr;image/vnd.xiff;application/vnd.xfdl;application/emma+xml;application/vnd.ezpix-album;application/vnd.ezpix-package;image/vnd.fst;video/vnd.fvt;image/vnd.fastbidsheet;application/vn.denovo.fcselayout-link;video/x-f4v;video/x-flv;image/vnd.fpx;image/vnd.net-fpx;text/vnd.fmi.flexstor;video/x-fli;application/vnd.fluxtime.clip;application/vnd.fdf;text/x-fortran;application/vnd.mif;application/vnd.framemaker;imae/x-freehand;application/vnd.fsc.weblaunch;application/vnd.frogans.fnc;application/vnd.frogans.ltf;application/vnd.fujixerox.ddd;application/vnd.fujixerox.docuworks;application/vnd.fujixerox.docuworks.binder;application/vnd.fujitu.oasys;application/vnd.fujitsu.oasys2;application/vnd.fujitsu.oasys3;application/vnd.fujitsu.oasysgp;application/vnd.fujitsu.oasysprs;application/x-futuresplash;application/vnd.fuzzysheet;image/g3fax;application/vnd.gmx;model/vn.gtw;application/vnd.genomatix.tuxedo;application/vnd.geogebra.file;application/vnd.geogebra.tool;model/vnd.gdl;application/vnd.geometry-explorer;application/vnd.geonext;application/vnd.geoplan;application/vnd.geospace;applicatio/x-font-ghostscript;application/x-font-bdf;application/x-gtar;application/x-texinfo;application/x-gnumeric;application/vnd.google-earth.kml+xml;application/vnd.google-earth.kmz;application/vnd.grafeq;image/gif;text/vnd.graphviz;aplication/vnd.groove-account;application/vnd.groove-help;application/vnd.groove-identity-message;application/vnd.groove-injector;application/vnd.groove-tool-message;application/vnd.groove-tool-template;application/vnd.groove-vcar;video/h261;video/h263;video/h264;application/vnd.hp-hpid;application/vnd.hp-hps;application/x-hdf;audio/vnd.rip;application/vnd.hbci;application/vnd.hp-jlyt;application/vnd.hp-pcl;application/vnd.hp-hpgl;application/vnd.yamaha.h-script;application/vnd.yamaha.hv-dic;application/vnd.yamaha.hv-voice;application/vnd.hydrostatix.sof-data;application/hyperstudio;application/vnd.hal+xml;text/html;application/vnd.ibm.rights-management;application/vnd.ibm.securecontainer;text/calendar;application/vnd.iccprofile;image/x-icon;application/vnd.igloader;image/ief;application/vnd.immervision-ivp;application/vnd.immervision-ivu;application/reginfo+xml;text/vnd.in3d.3dml;text/vnd.in3d.spot;mode/iges;application/vnd.intergeo;application/vnd.cinderella;application/vnd.intercon.formnet;application/vnd.isac.fcs;application/ipfix;application/pkix-cert;application/pkixcmp;application/pkix-crl;application/pkix-pkipath;applicaion/vnd.insors.igm;application/vnd.ipunplugged.rcprofile;application/vnd.irepository.package+xml;text/vnd.sun.j2me.app-descriptor;application/java-archive;application/java-vm;application/x-java-jnlp-file;application/java-serializd-object;text/x-java-source,java;application/javascript;application/json;application/vnd.joost.joda-archive;video/jpm;image/jpeg;video/jpeg;application/vnd.kahootz;application/vnd.chipnuts.karaoke-mmd;application/vnd.kde.karbon;aplication/vnd.kde.kchart;application/vnd.kde.kformula;application/vnd.kde.kivio;application/vnd.kde.kontour;application/vnd.kde.kpresenter;application/vnd.kde.kspread;application/vnd.kde.kword;application/vnd.kenameaapp;applicatin/vnd.kidspiration;application/vnd.kinar;application/vnd.kodak-descriptor;application/vnd.las.las+xml;application/x-latex;application/vnd.llamagraphics.life-balance.desktop;application/vnd.llamagraphics.life-balance.exchange+xml;application/vnd.jam;application/vnd.lotus-1-2-3;application/vnd.lotus-approach;application/vnd.lotus-freelance;application/vnd.lotus-notes;application/vnd.lotus-organizer;application/vnd.lotus-screencam;application/vnd.lotus-wordro;audio/vnd.lucent.voice;audio/x-mpegurl;video/x-m4v;application/mac-binhex40;application/vnd.macports.portpkg;application/vnd.osgeo.mapguide.package;application/marc;application/marcxml+xml;application/mxf;application/vnd.wolfrm.player;application/mathematica;application/mathml+xml;application/mbox;application/vnd.medcalcdata;application/mediaservercontrol+xml;application/vnd.mediastation.cdkey;application/vnd.mfer;application/vnd.mfmp;model/mesh;appliation/mads+xml;application/mets+xml;application/mods+xml;application/metalink4+xml;application/vnd.ms-powerpoint.template.macroenabled.12;application/vnd.ms-word.document.macroenabled.12;application/vnd.ms-word.template.macroenabed.12;application/vnd.mcd;application/vnd.micrografx.flo;application/vnd.micrografx.igx;application/vnd.eszigno3+xml;application/x-msaccess;video/x-ms-asf;application/x-msdownload;application/vnd.ms-artgalry;application/vnd.ms-ca-compressed;application/vnd.ms-ims;application/x-ms-application;application/x-msclip;image/vnd.ms-modi;application/vnd.ms-fontobject;application/vnd.ms-excel;application/vnd.ms-excel.addin.macroenabled.12;application/vnd.ms-excelsheet.binary.macroenabled.12;application/vnd.ms-excel.template.macroenabled.12;application/vnd.ms-excel.sheet.macroenabled.12;application/vnd.ms-htmlhelp;application/x-mscardfile;application/vnd.ms-lrm;application/x-msmediaview;aplication/x-msmoney;application/vnd.openxmlformats-officedocument.presentationml.presentation;application/vnd.openxmlformats-officedocument.presentationml.slide;application/vnd.openxmlformats-officedocument.presentationml.slideshw;application/vnd.openxmlformats-officedocument.presentationml.template;application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;application/vnd.openxmlformats-officedocument.spreadsheetml.template;application/vnd.openxmformats-officedocument.wordprocessingml.document;application/vnd.openxmlformats-officedocument.wordprocessingml.template;application/x-msbinder;application/vnd.ms-officetheme;application/onenote;audio/vnd.ms-playready.media.pya;vdeo/vnd.ms-playready.media.pyv;application/vnd.ms-powerpoint;application/vnd.ms-powerpoint.addin.macroenabled.12;application/vnd.ms-powerpoint.slide.macroenabled.12;application/vnd.ms-powerpoint.presentation.macroenabled.12;appliation/vnd.ms-powerpoint.slideshow.macroenabled.12;application/vnd.ms-project;application/x-mspublisher;application/x-msschedule;application/x-silverlight-app;application/vnd.ms-pki.stl;application/vnd.ms-pki.seccat;application/vn.visio;video/x-ms-wm;audio/x-ms-wma;audio/x-ms-wax;video/x-ms-wmx;application/x-ms-wmd;application/vnd.ms-wpl;application/x-ms-wmz;video/x-ms-wmv;video/x-ms-wvx;application/x-msmetafile;application/x-msterminal;application/msword;application/x-mswrite;application/vnd.ms-works;application/x-ms-xbap;application/vnd.ms-xpsdocument;audio/midi;application/vnd.ibm.minipay;application/vnd.ibm.modcap;application/vnd.jcp.javame.midlet-rms;application/vnd.tmobile-ivetv;application/x-mobipocket-ebook;application/vnd.mobius.mbk;application/vnd.mobius.dis;application/vnd.mobius.plc;application/vnd.mobius.mqy;application/vnd.mobius.msl;application/vnd.mobius.txf;application/vnd.mobius.daf;tex/vnd.fly;application/vnd.mophun.certificate;application/vnd.mophun.application;video/mj2;audio/mpeg;video/vnd.mpegurl;video/mpeg;application/mp21;audio/mp4;video/mp4;application/mp4;application/vnd.apple.mpegurl;application/vnd.msician;application/vnd.muvee.style;application/xv+xml;application/vnd.nokia.n-gage.data;application/vnd.nokia.n-gage.symbian.install;application/x-dtbncx+xml;application/x-netcdf;application/vnd.neurolanguage.nlu;application/vnd.na;application/vnd.noblenet-directory;application/vnd.noblenet-sealer;application/vnd.noblenet-web;application/vnd.nokia.radio-preset;application/vnd.nokia.radio-presets;text/n3;application/vnd.novadigm.edm;application/vnd.novadim.edx;application/vnd.novadigm.ext;application/vnd.flographit;audio/vnd.nuera.ecelp4800;audio/vnd.nuera.ecelp7470;audio/vnd.nuera.ecelp9600;application/oda;application/ogg;audio/ogg;video/ogg;application/vnd.oma.dd2+xml;applicatin/vnd.oasis.opendocument.text-web;application/oebps-package+xml;application/vnd.intu.qbo;application/vnd.openofficeorg.extension;application/vnd.yamaha.openscoreformat;audio/webm;video/webm;application/vnd.oasis.opendocument.char;application/vnd.oasis.opendocument.chart-template;application/vnd.oasis.opendocument.database;application/vnd.oasis.opendocument.formula;application/vnd.oasis.opendocument.formula-template;application/vnd.oasis.opendocument.grapics;application/vnd.oasis.opendocument.graphics-template;application/vnd.oasis.opendocument.image;application/vnd.oasis.opendocument.image-template;application/vnd.oasis.opendocument.presentation;application/vnd.oasis.opendocumen.presentation-template;application/vnd.oasis.opendocument.spreadsheet;application/vnd.oasis.opendocument.spreadsheet-template;application/vnd.oasis.opendocument.text;application/vnd.oasis.opendocument.text-master;application/vnd.asis.opendocument.text-template;image/ktx;application/vnd.sun.xml.calc;application/vnd.sun.xml.calc.template;application/vnd.sun.xml.draw;application/vnd.sun.xml.draw.template;application/vnd.sun.xml.impress;application/vnd.sun.xl.impress.template;application/vnd.sun.xml.math;application/vnd.sun.xml.writer;application/vnd.sun.xml.writer.global;application/vnd.sun.xml.writer.template;application/x-font-otf;application/vnd.yamaha.openscoreformat.osfpvg+xml;application/vnd.osgi.dp;application/vnd.palm;text/x-pascal;application/vnd.pawaafile;application/vnd.hp-pclxl;application/vnd.picsel;image/x-pcx;image/vnd.adobe.photoshop;application/pics-rules;image/x-pict;application/x-chat;aplication/pkcs10;application/x-pkcs12;application/pkcs7-mime;application/pkcs7-signature;application/x-pkcs7-certreqresp;application/x-pkcs7-certificates;application/pkcs8;application/vnd.pocketlearn;image/x-portable-anymap;image/-portable-bitmap;application/x-font-pcf;application/font-tdpfr;application/x-chess-pgn;image/x-portable-graymap;image/png;image/x-portable-pixmap;application/pskc+xml;application/vnd.ctc-posml;application/postscript;application/xfont-type1;application/vnd.powerbuilder6;application/pgp-encrypted;application/pgp-signature;application/vnd.previewsystems.box;application/vnd.pvi.ptid1;application/pls+xml;application/vnd.pg.format;application/vnd.pg.osasli;tex/prs.lines.tag;application/x-font-linux-psf;application/vnd.publishare-delta-tree;application/vnd.pmi.widget;application/vnd.quark.quarkxpress;application/vnd.epson.esf;application/vnd.epson.msf;application/vnd.epson.ssf;applicaton/vnd.epson.quickanime;application/vnd.intu.qfx;video/quicktime;application/x-rar-compressed;audio/x-pn-realaudio;audio/x-pn-realaudio-plugin;application/rsd+xml;application/vnd.rn-realmedia;application/vnd.realvnc.bed;applicatin/vnd.recordare.musicxml;application/vnd.recordare.musicxml+xml;application/relax-ng-compact-syntax;application/vnd.data-vision.rdz;application/rdf+xml;application/vnd.cloanto.rp9;application/vnd.jisp;application/rtf;text/richtex;application/vnd.route66.link66+xml;application/rss+xml;application/shf+xml;application/vnd.sailingtracker.track;image/svg+xml;application/vnd.sus-calendar;application/sru+xml;application/set-payment-initiation;application/set-reistration-initiation;application/vnd.sema;application/vnd.semd;application/vnd.semf;application/vnd.seemail;application/x-font-snf;application/scvp-vp-request;application/scvp-vp-response;application/scvp-cv-request;application/svp-cv-response;application/sdp;text/x-setext;video/x-sgi-movie;application/vnd.shana.informed.formdata;application/vnd.shana.informed.formtemplate;application/vnd.shana.informed.interchange;application/vnd.shana.informed.package;application/thraud+xml;application/x-shar;image/x-rgb;application/vnd.epson.salt;application/vnd.accpac.simply.aso;application/vnd.accpac.simply.imp;application/vnd.simtech-mindmapper;application/vnd.commonspace;application/vnd.ymaha.smaf-audio;application/vnd.smaf;application/vnd.yamaha.smaf-phrase;application/vnd.smart.teacher;application/vnd.svd;application/sparql-query;application/sparql-results+xml;application/srgs;application/srgs+xml;application/sml+xml;application/vnd.koan;text/sgml;application/vnd.stardivision.calc;application/vnd.stardivision.draw;application/vnd.stardivision.impress;application/vnd.stardivision.math;application/vnd.stardivision.writer;application/vnd.tardivision.writer-global;application/vnd.stepmania.stepchart;application/x-stuffit;application/x-stuffitx;application/vnd.solent.sdkm+xml;application/vnd.olpc-sugar;audio/basic;application/vnd.wqd;application/vnd.symbian.install;application/smil+xml;application/vnd.syncml+xml;application/vnd.syncml.dm+wbxml;application/vnd.syncml.dm+xml;application/x-sv4cpio;application/x-sv4crc;application/sbml+xml;text/tab-separated-values;image/tiff;application/vnd.to.intent-module-archive;application/x-tar;application/x-tcl;application/x-tex;application/x-tex-tfm;application/tei+xml;text/plain;application/vnd.spotfire.dxp;application/vnd.spotfire.sfs;application/timestamped-data;applicationvnd.trid.tpt;application/vnd.triscape.mxs;text/troff;application/vnd.trueapp;application/x-font-ttf;text/turtle;application/vnd.umajin;application/vnd.uoml+xml;application/vnd.unity;application/vnd.ufdl;text/uri-list;application/nd.uiq.theme;application/x-ustar;text/x-uuencode;text/x-vcalendar;text/x-vcard;application/x-cdlink;application/vnd.vsf;model/vrml;application/vnd.vcx;model/vnd.mts;model/vnd.vtu;application/vnd.visionary;video/vnd.vivo;applicatin/ccxml+xml,;application/voicexml+xml;application/x-wais-source;application/vnd.wap.wbxml;image/vnd.wap.wbmp;audio/x-wav;application/davmount+xml;application/x-font-woff;application/wspolicy+xml;image/webp;application/vnd.webturb;application/widget;application/winhlp;text/vnd.wap.wml;text/vnd.wap.wmlscript;application/vnd.wap.wmlscriptc;application/vnd.wordperfect;application/vnd.wt.stf;application/wsdl+xml;image/x-xbitmap;image/x-xpixmap;image/x-xwindowump;application/x-x509-ca-cert;application/x-xfig;application/xhtml+xml;application/xml;application/xcap-diff+xml;application/xenc+xml;application/patch-ops-error+xml;application/resource-lists+xml;application/rls-services+xml;aplication/resource-lists-diff+xml;application/xslt+xml;application/xop+xml;application/x-xpinstall;application/xspf+xml;application/vnd.mozilla.xul+xml;chemical/x-xyz;text/yaml;application/yang;application/yin+xml;application/vnd.ul;application/zip;application/vnd.handheld-entertainment+xml;application/vnd.zzazz.deck+xml")

    from selenium.webdriver import FirefoxOptions

    opts = FirefoxOptions()
    opts.add_argument("--headless")
    proxy_server = '--proxy-server=' + proxy
    opts.add_argument(proxy_server)
    binary = FirefoxBinary('/usr/bin/firefox')
    driver = webdriver.Firefox(firefox_binary=binary,
                               executable_path="/usr/local/bin/geckodriver", firefox_options=opts, firefox_profile=fp)

    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))
    Words = Get_wordsTrends(conn)

    period = '1-d'
    for w in Words:

        sql = """SELECT date_period FROM google.trends where words='test' order by date_period desc limit 1;"""
        sql = sql.replace('test', w)

        try:
            cur = conn.cursor()
            # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
            cur.execute(sql)
            data = cur.fetchall()
        except psycopg2.Error as err:
            print("Query error: {}".format(err))

        data_end = datetime.strptime(data[0][0], '%Y-%m-%d %H:%M:%S')
        ##  temp_date=datetime.strftime('2018-05-22 00:00:00','%Y-%m-%d %H:%M:%S')
        if str(data_end) == '2018-05-22 00:00:00':
            continue
        if len(data) == 0:
            continue
        Load_Csv('3-m', w, driver)
        Data = Get_Csv('/www/telegram/File/')
        for i in range(0, len(Data) - 1):
            try:
                data_temp = datetime.strptime(Data[i][0] + ' 00:00', '%Y-%m-%d %H:%M')

                if data_temp < data_end:
                    continue
                sql = """INSERT INTO google.trends(words, period, date_period, value1, value2, value3, value4, value5)VALUES("""
                if re.search('<', Data[i][1]):
                    Data[i][1] = 1
                if re.search('<', Data[i][2]):
                    Data[i][2] = 1
                if re.search('<', Data[i][3]):
                    Data[i][3] = 1
                if re.search('<', Data[i][4]):
                    Data[i][4] = 1
                if re.search('<', Data[i][5]):
                    Data[i][5] = 1

                sql = sql + '\'' + w + '\',' + '\'' + period + '\',' + '\'' + str(data_temp) + '\',' + str(
                    Data[i][1]) + ',' + str(Data[i][2]) + ',' + str(Data[i][3]) + ',' + str(Data[i][4]) + ',' + str(
                    Data[i][5]) + ');'

            except:
                break
            try:
                cur = conn.cursor()
                # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
                cur.execute(sql)
                conn.commit()
            except psycopg2.Error as err:
                print("Query error: {}".format(err))
        print(w)
    return True


def GetMaxTitle(period):
    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))

    Words = Get_wordsTrends()
    max = 0


def Driver(index, path):
    with open('/www/telegram/Proxy.txt', 'r', encoding='utf8') as file:
        proxy = file.read()
    index = 0
    fp = webdriver.FirefoxProfile()

    fp.set_preference("browser.download.folderList", 2)

    fp.set_preference("browser.download.manager.showWhenStarting", False)
    fp.set_preference("browser.download.defaultFolder", path)
    fp.set_preference("browser.download.dir", path)
    fp.set_preference("browser.helperApps.neverAsk.saveToDisk",
                      "application/vnd.hzn-3d-crossword;video/3gpp;video/3gpp2;application/vnd.mseq;application/vnd.3m.post-it-notes;application/vnd.3gpp.pic-bw-large;application/vnd.3gpp.pic-bw-small;application/vnd.3gpp.pic-bw-var;application/vnd.3gp2.tcap;application/x-7z-compressed;application/x-abiword;application/x-ace-compressed;application/vnd.americandynamics.acc;application/vnd.acucobol;application/vnd.acucorp;audio/adpcm;application/x-authorware-bin;application/x-athorware-map;application/x-authorware-seg;application/vnd.adobe.air-application-installer-package+zip;application/x-shockwave-flash;application/vnd.adobe.fxp;application/pdf;application/vnd.cups-ppd;application/x-director;applicaion/vnd.adobe.xdp+xml;application/vnd.adobe.xfdf;audio/x-aac;application/vnd.ahead.space;application/vnd.airzip.filesecure.azf;application/vnd.airzip.filesecure.azs;application/vnd.amazon.ebook;application/vnd.amiga.ami;applicatin/andrew-inset;application/vnd.android.package-archive;application/vnd.anser-web-certificate-issue-initiation;application/vnd.anser-web-funds-transfer-initiation;application/vnd.antix.game-component;application/vnd.apple.installe+xml;application/applixware;application/vnd.hhe.lesson-player;application/vnd.aristanetworks.swi;text/x-asm;application/atomcat+xml;application/atomsvc+xml;application/atom+xml;application/pkix-attr-cert;audio/x-aiff;video/x-msvieo;application/vnd.audiograph;image/vnd.dxf;model/vnd.dwf;text/plain-bas;application/x-bcpio;application/octet-stream;image/bmp;application/x-bittorrent;application/vnd.rim.cod;application/vnd.blueice.multipass;application/vnd.bm;application/x-sh;image/prs.btif;application/vnd.businessobjects;application/x-bzip;application/x-bzip2;application/x-csh;text/x-c;application/vnd.chemdraw+xml;text/css;chemical/x-cdx;chemical/x-cml;chemical/x-csml;application/vn.contact.cmsg;application/vnd.claymore;application/vnd.clonk.c4group;image/vnd.dvb.subtitle;application/cdmi-capability;application/cdmi-container;application/cdmi-domain;application/cdmi-object;application/cdmi-queue;applicationvnd.cluetrust.cartomobile-config;application/vnd.cluetrust.cartomobile-config-pkg;image/x-cmu-raster;model/vnd.collada+xml;text/csv;application/mac-compactpro;application/vnd.wap.wmlc;image/cgm;x-conference/x-cooltalk;image/x-cmx;application/vnd.xara;application/vnd.cosmocaller;application/x-cpio;application/vnd.crick.clicker;application/vnd.crick.clicker.keyboard;application/vnd.crick.clicker.palette;application/vnd.crick.clicker.template;application/vn.crick.clicker.wordbank;application/vnd.criticaltools.wbs+xml;application/vnd.rig.cryptonote;chemical/x-cif;chemical/x-cmdf;application/cu-seeme;application/prs.cww;text/vnd.curl;text/vnd.curl.dcurl;text/vnd.curl.mcurl;text/vnd.crl.scurl;application/vnd.curl.car;application/vnd.curl.pcurl;application/vnd.yellowriver-custom-menu;application/dssc+der;application/dssc+xml;application/x-debian-package;audio/vnd.dece.audio;image/vnd.dece.graphic;video/vnd.dec.hd;video/vnd.dece.mobile;video/vnd.uvvu.mp4;video/vnd.dece.pd;video/vnd.dece.sd;video/vnd.dece.video;application/x-dvi;application/vnd.fdsn.seed;application/x-dtbook+xml;application/x-dtbresource+xml;application/vnd.dvb.ait;applcation/vnd.dvb.service;audio/vnd.digital-winds;image/vnd.djvu;application/xml-dtd;application/vnd.dolby.mlp;application/x-doom;application/vnd.dpgraph;audio/vnd.dra;application/vnd.dreamfactory;audio/vnd.dts;audio/vnd.dts.hd;imag/vnd.dwg;application/vnd.dynageo;application/ecmascript;application/vnd.ecowin.chart;image/vnd.fujixerox.edmics-mmr;image/vnd.fujixerox.edmics-rlc;application/exi;application/vnd.proteus.magazine;application/epub+zip;message/rfc82;application/vnd.enliven;application/vnd.is-xpr;image/vnd.xiff;application/vnd.xfdl;application/emma+xml;application/vnd.ezpix-album;application/vnd.ezpix-package;image/vnd.fst;video/vnd.fvt;image/vnd.fastbidsheet;application/vn.denovo.fcselayout-link;video/x-f4v;video/x-flv;image/vnd.fpx;image/vnd.net-fpx;text/vnd.fmi.flexstor;video/x-fli;application/vnd.fluxtime.clip;application/vnd.fdf;text/x-fortran;application/vnd.mif;application/vnd.framemaker;imae/x-freehand;application/vnd.fsc.weblaunch;application/vnd.frogans.fnc;application/vnd.frogans.ltf;application/vnd.fujixerox.ddd;application/vnd.fujixerox.docuworks;application/vnd.fujixerox.docuworks.binder;application/vnd.fujitu.oasys;application/vnd.fujitsu.oasys2;application/vnd.fujitsu.oasys3;application/vnd.fujitsu.oasysgp;application/vnd.fujitsu.oasysprs;application/x-futuresplash;application/vnd.fuzzysheet;image/g3fax;application/vnd.gmx;model/vn.gtw;application/vnd.genomatix.tuxedo;application/vnd.geogebra.file;application/vnd.geogebra.tool;model/vnd.gdl;application/vnd.geometry-explorer;application/vnd.geonext;application/vnd.geoplan;application/vnd.geospace;applicatio/x-font-ghostscript;application/x-font-bdf;application/x-gtar;application/x-texinfo;application/x-gnumeric;application/vnd.google-earth.kml+xml;application/vnd.google-earth.kmz;application/vnd.grafeq;image/gif;text/vnd.graphviz;aplication/vnd.groove-account;application/vnd.groove-help;application/vnd.groove-identity-message;application/vnd.groove-injector;application/vnd.groove-tool-message;application/vnd.groove-tool-template;application/vnd.groove-vcar;video/h261;video/h263;video/h264;application/vnd.hp-hpid;application/vnd.hp-hps;application/x-hdf;audio/vnd.rip;application/vnd.hbci;application/vnd.hp-jlyt;application/vnd.hp-pcl;application/vnd.hp-hpgl;application/vnd.yamaha.h-script;application/vnd.yamaha.hv-dic;application/vnd.yamaha.hv-voice;application/vnd.hydrostatix.sof-data;application/hyperstudio;application/vnd.hal+xml;text/html;application/vnd.ibm.rights-management;application/vnd.ibm.securecontainer;text/calendar;application/vnd.iccprofile;image/x-icon;application/vnd.igloader;image/ief;application/vnd.immervision-ivp;application/vnd.immervision-ivu;application/reginfo+xml;text/vnd.in3d.3dml;text/vnd.in3d.spot;mode/iges;application/vnd.intergeo;application/vnd.cinderella;application/vnd.intercon.formnet;application/vnd.isac.fcs;application/ipfix;application/pkix-cert;application/pkixcmp;application/pkix-crl;application/pkix-pkipath;applicaion/vnd.insors.igm;application/vnd.ipunplugged.rcprofile;application/vnd.irepository.package+xml;text/vnd.sun.j2me.app-descriptor;application/java-archive;application/java-vm;application/x-java-jnlp-file;application/java-serializd-object;text/x-java-source,java;application/javascript;application/json;application/vnd.joost.joda-archive;video/jpm;image/jpeg;video/jpeg;application/vnd.kahootz;application/vnd.chipnuts.karaoke-mmd;application/vnd.kde.karbon;aplication/vnd.kde.kchart;application/vnd.kde.kformula;application/vnd.kde.kivio;application/vnd.kde.kontour;application/vnd.kde.kpresenter;application/vnd.kde.kspread;application/vnd.kde.kword;application/vnd.kenameaapp;applicatin/vnd.kidspiration;application/vnd.kinar;application/vnd.kodak-descriptor;application/vnd.las.las+xml;application/x-latex;application/vnd.llamagraphics.life-balance.desktop;application/vnd.llamagraphics.life-balance.exchange+xml;application/vnd.jam;application/vnd.lotus-1-2-3;application/vnd.lotus-approach;application/vnd.lotus-freelance;application/vnd.lotus-notes;application/vnd.lotus-organizer;application/vnd.lotus-screencam;application/vnd.lotus-wordro;audio/vnd.lucent.voice;audio/x-mpegurl;video/x-m4v;application/mac-binhex40;application/vnd.macports.portpkg;application/vnd.osgeo.mapguide.package;application/marc;application/marcxml+xml;application/mxf;application/vnd.wolfrm.player;application/mathematica;application/mathml+xml;application/mbox;application/vnd.medcalcdata;application/mediaservercontrol+xml;application/vnd.mediastation.cdkey;application/vnd.mfer;application/vnd.mfmp;model/mesh;appliation/mads+xml;application/mets+xml;application/mods+xml;application/metalink4+xml;application/vnd.ms-powerpoint.template.macroenabled.12;application/vnd.ms-word.document.macroenabled.12;application/vnd.ms-word.template.macroenabed.12;application/vnd.mcd;application/vnd.micrografx.flo;application/vnd.micrografx.igx;application/vnd.eszigno3+xml;application/x-msaccess;video/x-ms-asf;application/x-msdownload;application/vnd.ms-artgalry;application/vnd.ms-ca-compressed;application/vnd.ms-ims;application/x-ms-application;application/x-msclip;image/vnd.ms-modi;application/vnd.ms-fontobject;application/vnd.ms-excel;application/vnd.ms-excel.addin.macroenabled.12;application/vnd.ms-excelsheet.binary.macroenabled.12;application/vnd.ms-excel.template.macroenabled.12;application/vnd.ms-excel.sheet.macroenabled.12;application/vnd.ms-htmlhelp;application/x-mscardfile;application/vnd.ms-lrm;application/x-msmediaview;aplication/x-msmoney;application/vnd.openxmlformats-officedocument.presentationml.presentation;application/vnd.openxmlformats-officedocument.presentationml.slide;application/vnd.openxmlformats-officedocument.presentationml.slideshw;application/vnd.openxmlformats-officedocument.presentationml.template;application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;application/vnd.openxmlformats-officedocument.spreadsheetml.template;application/vnd.openxmformats-officedocument.wordprocessingml.document;application/vnd.openxmlformats-officedocument.wordprocessingml.template;application/x-msbinder;application/vnd.ms-officetheme;application/onenote;audio/vnd.ms-playready.media.pya;vdeo/vnd.ms-playready.media.pyv;application/vnd.ms-powerpoint;application/vnd.ms-powerpoint.addin.macroenabled.12;application/vnd.ms-powerpoint.slide.macroenabled.12;application/vnd.ms-powerpoint.presentation.macroenabled.12;appliation/vnd.ms-powerpoint.slideshow.macroenabled.12;application/vnd.ms-project;application/x-mspublisher;application/x-msschedule;application/x-silverlight-app;application/vnd.ms-pki.stl;application/vnd.ms-pki.seccat;application/vn.visio;video/x-ms-wm;audio/x-ms-wma;audio/x-ms-wax;video/x-ms-wmx;application/x-ms-wmd;application/vnd.ms-wpl;application/x-ms-wmz;video/x-ms-wmv;video/x-ms-wvx;application/x-msmetafile;application/x-msterminal;application/msword;application/x-mswrite;application/vnd.ms-works;application/x-ms-xbap;application/vnd.ms-xpsdocument;audio/midi;application/vnd.ibm.minipay;application/vnd.ibm.modcap;application/vnd.jcp.javame.midlet-rms;application/vnd.tmobile-ivetv;application/x-mobipocket-ebook;application/vnd.mobius.mbk;application/vnd.mobius.dis;application/vnd.mobius.plc;application/vnd.mobius.mqy;application/vnd.mobius.msl;application/vnd.mobius.txf;application/vnd.mobius.daf;tex/vnd.fly;application/vnd.mophun.certificate;application/vnd.mophun.application;video/mj2;audio/mpeg;video/vnd.mpegurl;video/mpeg;application/mp21;audio/mp4;video/mp4;application/mp4;application/vnd.apple.mpegurl;application/vnd.msician;application/vnd.muvee.style;application/xv+xml;application/vnd.nokia.n-gage.data;application/vnd.nokia.n-gage.symbian.install;application/x-dtbncx+xml;application/x-netcdf;application/vnd.neurolanguage.nlu;application/vnd.na;application/vnd.noblenet-directory;application/vnd.noblenet-sealer;application/vnd.noblenet-web;application/vnd.nokia.radio-preset;application/vnd.nokia.radio-presets;text/n3;application/vnd.novadigm.edm;application/vnd.novadim.edx;application/vnd.novadigm.ext;application/vnd.flographit;audio/vnd.nuera.ecelp4800;audio/vnd.nuera.ecelp7470;audio/vnd.nuera.ecelp9600;application/oda;application/ogg;audio/ogg;video/ogg;application/vnd.oma.dd2+xml;applicatin/vnd.oasis.opendocument.text-web;application/oebps-package+xml;application/vnd.intu.qbo;application/vnd.openofficeorg.extension;application/vnd.yamaha.openscoreformat;audio/webm;video/webm;application/vnd.oasis.opendocument.char;application/vnd.oasis.opendocument.chart-template;application/vnd.oasis.opendocument.database;application/vnd.oasis.opendocument.formula;application/vnd.oasis.opendocument.formula-template;application/vnd.oasis.opendocument.grapics;application/vnd.oasis.opendocument.graphics-template;application/vnd.oasis.opendocument.image;application/vnd.oasis.opendocument.image-template;application/vnd.oasis.opendocument.presentation;application/vnd.oasis.opendocumen.presentation-template;application/vnd.oasis.opendocument.spreadsheet;application/vnd.oasis.opendocument.spreadsheet-template;application/vnd.oasis.opendocument.text;application/vnd.oasis.opendocument.text-master;application/vnd.asis.opendocument.text-template;image/ktx;application/vnd.sun.xml.calc;application/vnd.sun.xml.calc.template;application/vnd.sun.xml.draw;application/vnd.sun.xml.draw.template;application/vnd.sun.xml.impress;application/vnd.sun.xl.impress.template;application/vnd.sun.xml.math;application/vnd.sun.xml.writer;application/vnd.sun.xml.writer.global;application/vnd.sun.xml.writer.template;application/x-font-otf;application/vnd.yamaha.openscoreformat.osfpvg+xml;application/vnd.osgi.dp;application/vnd.palm;text/x-pascal;application/vnd.pawaafile;application/vnd.hp-pclxl;application/vnd.picsel;image/x-pcx;image/vnd.adobe.photoshop;application/pics-rules;image/x-pict;application/x-chat;aplication/pkcs10;application/x-pkcs12;application/pkcs7-mime;application/pkcs7-signature;application/x-pkcs7-certreqresp;application/x-pkcs7-certificates;application/pkcs8;application/vnd.pocketlearn;image/x-portable-anymap;image/-portable-bitmap;application/x-font-pcf;application/font-tdpfr;application/x-chess-pgn;image/x-portable-graymap;image/png;image/x-portable-pixmap;application/pskc+xml;application/vnd.ctc-posml;application/postscript;application/xfont-type1;application/vnd.powerbuilder6;application/pgp-encrypted;application/pgp-signature;application/vnd.previewsystems.box;application/vnd.pvi.ptid1;application/pls+xml;application/vnd.pg.format;application/vnd.pg.osasli;tex/prs.lines.tag;application/x-font-linux-psf;application/vnd.publishare-delta-tree;application/vnd.pmi.widget;application/vnd.quark.quarkxpress;application/vnd.epson.esf;application/vnd.epson.msf;application/vnd.epson.ssf;applicaton/vnd.epson.quickanime;application/vnd.intu.qfx;video/quicktime;application/x-rar-compressed;audio/x-pn-realaudio;audio/x-pn-realaudio-plugin;application/rsd+xml;application/vnd.rn-realmedia;application/vnd.realvnc.bed;applicatin/vnd.recordare.musicxml;application/vnd.recordare.musicxml+xml;application/relax-ng-compact-syntax;application/vnd.data-vision.rdz;application/rdf+xml;application/vnd.cloanto.rp9;application/vnd.jisp;application/rtf;text/richtex;application/vnd.route66.link66+xml;application/rss+xml;application/shf+xml;application/vnd.sailingtracker.track;image/svg+xml;application/vnd.sus-calendar;application/sru+xml;application/set-payment-initiation;application/set-reistration-initiation;application/vnd.sema;application/vnd.semd;application/vnd.semf;application/vnd.seemail;application/x-font-snf;application/scvp-vp-request;application/scvp-vp-response;application/scvp-cv-request;application/svp-cv-response;application/sdp;text/x-setext;video/x-sgi-movie;application/vnd.shana.informed.formdata;application/vnd.shana.informed.formtemplate;application/vnd.shana.informed.interchange;application/vnd.shana.informed.package;application/thraud+xml;application/x-shar;image/x-rgb;application/vnd.epson.salt;application/vnd.accpac.simply.aso;application/vnd.accpac.simply.imp;application/vnd.simtech-mindmapper;application/vnd.commonspace;application/vnd.ymaha.smaf-audio;application/vnd.smaf;application/vnd.yamaha.smaf-phrase;application/vnd.smart.teacher;application/vnd.svd;application/sparql-query;application/sparql-results+xml;application/srgs;application/srgs+xml;application/sml+xml;application/vnd.koan;text/sgml;application/vnd.stardivision.calc;application/vnd.stardivision.draw;application/vnd.stardivision.impress;application/vnd.stardivision.math;application/vnd.stardivision.writer;application/vnd.tardivision.writer-global;application/vnd.stepmania.stepchart;application/x-stuffit;application/x-stuffitx;application/vnd.solent.sdkm+xml;application/vnd.olpc-sugar;audio/basic;application/vnd.wqd;application/vnd.symbian.install;application/smil+xml;application/vnd.syncml+xml;application/vnd.syncml.dm+wbxml;application/vnd.syncml.dm+xml;application/x-sv4cpio;application/x-sv4crc;application/sbml+xml;text/tab-separated-values;image/tiff;application/vnd.to.intent-module-archive;application/x-tar;application/x-tcl;application/x-tex;application/x-tex-tfm;application/tei+xml;text/plain;application/vnd.spotfire.dxp;application/vnd.spotfire.sfs;application/timestamped-data;applicationvnd.trid.tpt;application/vnd.triscape.mxs;text/troff;application/vnd.trueapp;application/x-font-ttf;text/turtle;application/vnd.umajin;application/vnd.uoml+xml;application/vnd.unity;application/vnd.ufdl;text/uri-list;application/nd.uiq.theme;application/x-ustar;text/x-uuencode;text/x-vcalendar;text/x-vcard;application/x-cdlink;application/vnd.vsf;model/vrml;application/vnd.vcx;model/vnd.mts;model/vnd.vtu;application/vnd.visionary;video/vnd.vivo;applicatin/ccxml+xml,;application/voicexml+xml;application/x-wais-source;application/vnd.wap.wbxml;image/vnd.wap.wbmp;audio/x-wav;application/davmount+xml;application/x-font-woff;application/wspolicy+xml;image/webp;application/vnd.webturb;application/widget;application/winhlp;text/vnd.wap.wml;text/vnd.wap.wmlscript;application/vnd.wap.wmlscriptc;application/vnd.wordperfect;application/vnd.wt.stf;application/wsdl+xml;image/x-xbitmap;image/x-xpixmap;image/x-xwindowump;application/x-x509-ca-cert;application/x-xfig;application/xhtml+xml;application/xml;application/xcap-diff+xml;application/xenc+xml;application/patch-ops-error+xml;application/resource-lists+xml;application/rls-services+xml;aplication/resource-lists-diff+xml;application/xslt+xml;application/xop+xml;application/x-xpinstall;application/xspf+xml;application/vnd.mozilla.xul+xml;chemical/x-xyz;text/yaml;application/yang;application/yin+xml;application/vnd.ul;application/zip;application/vnd.handheld-entertainment+xml;application/vnd.zzazz.deck+xml")

    from selenium.webdriver import FirefoxOptions

    opts = FirefoxOptions()
    opts.add_argument("--headless")
    proxy_server='--proxy-server='+proxy[index]
    ##proxy_server = '--proxy-server=165.16.3.54:53281'
    opts.add_argument(proxy_server)
    binary = FirefoxBinary('/usr/bin/firefox')
    driver = webdriver.Firefox(firefox_options=opts, firefox_profile=fp)
    return driver


def Load_TrendsOne(df, name):
    index = 0
    i = 0
    main = []
    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))
    driver = Driver(index, '/www/telegram/File/')
    words = ''
    flag = False
    w = df[0]
    for ww in w:
        flag = False
        if ww == 0:
            continue

        else:
            i += 1
            if i < 5:
                ww = ww.lower()

                Load_Csv('1-m', ww, driver, '/www/telegram/File/')
                Data = Get_Csv('/www/telegram/File/')
                main.append(Data)
            words += ww + ','
            max = ww
        if i == 5:

            words = words[0:len(words) - 1]
            max = words.split(',')[4]
            Load_Csv('1-m', words, driver, '/www/telegram/File/')
            Data = Get_Csv('/www/telegram/File/')
            words = words.split(',')

            for i in range(0, len(Data) - 2):
                try:
                    date_start = datetime.strptime(Data[i][0] + ' 00:00:00', '%Y-%m-%d %H:%M:%S')
                except:
                    continue

                for j in range(0, 4):
                    try:
                        sql = '''SELECT processing_dttm FROM google.trends_word where word='xptx' order by processing_dttm desc limit 1;'''
                        sql = sql.replace('xptx', words[j])
                        ## try:
                        cur = conn.cursor()
                        # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
                        cur.execute(sql)
                        data = cur.fetchall()
                        '''
                        except psycopg2.Error as err:
                            print("Query error: {}".format(err))
                        '''
                        if len(data) == 0:
                            continue
                        date_end = data[0][0]
                        if date_start <= date_end:
                            continue
                        sql = '''INSERT INTO google.trends_word(word, direct, "relative", concerning_word, processing_dttm)VALUES('''

                        if re.search('<', Data[i][1]):
                            Data[i][1] = 1
                        if re.search('<', Data[i][2]):
                            Data[i][2] = 1
                        if re.search('<', Data[i][3]):
                            Data[i][3] = 1
                        if re.search('<', Data[i][4]):
                            Data[i][4] = 1
                        if re.search('<', Data[i][5]):
                            Data[i][5] = 1

                        sql = sql + '\'' + words[j] + '\',' + str(main[j][i][1]) + ',' + str(
                            Data[i][j + 1]) + ',' + '\'' + max + '\',' + '\'' + str(date_start) + '\');'
                    except:
                        continue
                    try:
                        cur = conn.cursor()
                        # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
                        cur.execute(sql)
                        conn.commit()
                    except psycopg2.Error as err:
                        print("Query error: {}".format(err))
            print(w)
            flag = True

        if flag == True:
            i = 1
            words = max + ','
            main = []
            Load_Csv('1-m', max, driver, '/www/telegram/File/')
            Data = Get_Csv('/www/telegram/File/')
            main.append(Data)
    words = words[0:len(words) - 1]

    Load_Csv('1-m', words, driver)
    Data = Get_Csv('/www/telegram//File//')
    words = words.split(',')
    for i in range(0, len(Data) - 1):
        try:
            date_start = datetime.strptime(Data[i][0] + ' 00:00:00', '%Y-%m-%d %H:%M:%S')
        except:
            continue
        for j in range(0, 4):
            try:
                sql = '''INSERT INTO google.trends_word(word, direct, "relative", concerning_word, processing_dttm)VALUES('''

                if re.search('<', Data[i][1]):
                    Data[i][1] = 1
                if re.search('<', Data[i][2]):
                    Data[i][2] = 1
                if re.search('<', Data[i][3]):
                    Data[i][3] = 1
                if re.search('<', Data[i][4]):
                    Data[i][4] = 1
                if re.search('<', Data[i][5]):
                    Data[i][5] = 1

                sql = sql + '\'' + words[j] + '\',' + str(main[j][i][1]) + ',' + str(
                    Data[i][j + 1]) + ',' + '\'' + max + '\',' + '\'' + str(date_start) + '\');'
            except:
                break
            try:
                cur = conn.cursor()
                # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
                cur.execute(sql)
                conn.commit()
            except psycopg2.Error as err:
                print("Query error: {}".format(err))
    print(w)


def Load_TrendsDay(file):
    Date_Start=datetime.now()
    Date_Start=datetime.strptime(str(Date_Start.year)+'-'+str(Date_Start.month)+'-'+str(Date_Start.day)+' 00:00:00','%Y-%m-%d %H:%M:%S')


    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))
    sql='''SELECT count(word)
FROM google.trends_day
where ddtm='Data-Start';'''
    sql=sql.replace('Data-Start',str(Date_Start))
    cur = conn.cursor()
    # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
    cur.execute(sql)
    count = cur.fetchall()
    count=count[0][0]/4

    for ii in range(0,4):
        try:
         driver = Driver(ii, '/www/telegram/File1/')


         Words=Get_words(conn,str(Date_Start))

         c=0
         for w in Words:

           flag=False
           w = w.lower()
           d=w.split(',')
           d.remove('bitcoin')
           Date_Start = datetime.now()
           Date_Start = Date_Start - timedelta(days=1)
           date_start = datetime.strptime(
            str(Date_Start.year) + '-' + str(Date_Start.month) + '-' + str(Date_Start.day) + ' 00:00:00',
            '%Y-%m-%d %H:%M:%S')
           for i in range(0,4):
            sql = '''SELECT word FROM  google.trends_day where ddtm = 'test2' and word = 'test1';'''
            sql = sql.replace('test2', str(date_start)).replace('test1', d[i])
            cur = conn.cursor()
            # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
            cur.execute(sql)
            zz = cur.fetchall()
            if len(zz) > 0:
                flag=True
                break

           if flag==True:
            continue


           file.writelines(w)
           print (w)
           Load_CsvDay('7-d', w, driver, '/www/telegram/File1/')
           Data = Get_Csv('/www/telegram/File1/')
           w = w.split(',')

           w.remove('bitcoin')

           D = [0, 0, 0, 0]
           Date_Start = datetime.now()
           Date_Start = Date_Start - timedelta(days=7)
           Date_Start = datetime.strptime(
            str(Date_Start.year) + '-' + str(Date_Start.month) + '-' + str(Date_Start.day) + ' 00:00:00',
            '%Y-%m-%d %H:%M:%S')




           for i in range(0, len(Data) - 2):
            try:
                flag = False
                date_start = datetime.strptime(Data[i][0].split('T')[0] + ' 00:00:00', '%Y-%m-%d %H:%M:%S')

            except:
                continue

            for j in range(0, 4):
                sql = '''SELECT word FROM  google.trends_day where ddtm = 'test2' and word = 'test1';'''
                sql = sql.replace('test2', str(Date_Start)).replace('test1', w[j])
                cur = conn.cursor()
                # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
                cur.execute(sql)
                zz = cur.fetchall()
                if re.search('<',str(Data[i][j+2])):
                    Data[i][j+2]=1
                if date_start == Date_Start:

                    D[j] += int(Data[i][j + 2])

                else:

                    if len(zz) != 0:
                        for zz in range(2,6):
                            if re.search('<',str(Data[i][zz])):
                                Data[i][zz]=1
                        D = [int(Data[i][2]), int(Data[i][3]), int(Data[i][4]), int(Data[i][5])]
                        Date_Start = Date_Start + timedelta(days=1)
                        break
                    for z in range(0, 4):


                         sql = '''INSERT INTO google.trends_day (word, value, ddtm) VALUES('''

                         sql = sql + '\'' + w[z] + '\',' + str(D[z]) + ',\''  + str(Date_Start) + '\');'

                         try:
                            cur = conn.cursor()
                            # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
                            cur.execute(sql)
                            conn.commit()

                         except psycopg2.Error as err:
                            print("Query error: {}".format(err))

                    for zz in range(2, 6):
                        if re.search('<', str(Data[i][zz])):
                            Data[i][zz] = 1
                    D = [int(Data[i][2]), int(Data[i][3]), int(Data[i][4]), int(Data[i][5])]
                    Date_Start = Date_Start + timedelta(days=1)



##        except:

  ##          continue
        except:
            continue


def LoadBitcoin():
    with open('/www/telegram/Proxy.txt', 'r', encoding='utf8') as file:
        proxy = file.read()
    index = 0
    w = 'bitcoin'
    driver = Driver(index, '/www/telegram/File1/')
    Load_Csv('1-m', w, driver, '/www/telegram/File1/')
    Data = Get_Csv('/www/telegram//File1/')
    max = Data[0][1]
    index = 0
    for i in range(1, len(Data) - 1):
        if max < Data[i][1]:
            max = Data[i][i]
            index = i
    weight = []
    for d in Data:
        w = {}
        w['weight'] = d[1] / max
        w['date'] = d[0]

        weight.append(w)



##LoadBitcoin()
##aver=average(7,'symbol')
##index=0
with open('/www/telegram/test1.txt', 'w', encoding='utf8') as file:
    file.write('Hello world!')
    Load_TrendsDay(file)
    file.close()

##Load_TrendsOne(aver,'symbol')
##Load_Day()

##Load_data('symbol')

'''
with open('/www/telegram/test1.txt', 'w', encoding='utf8') as file:
   file.
index = 0
driver = Driver(index, '/www/telegram/File/')
Load(driver)
'''