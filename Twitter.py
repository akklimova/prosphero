from bs4 import BeautifulSoup
import urllib3
import datetime
import re
import psycopg2


def Settings():
    file = 'D://settings.txt'
    with open(file, 'r', encoding='utf8') as f:
        data = f.read()
        data = data.split('\n')
        setting = {}
        setting['host'] = data[0].replace('host:', '').replace(' ', '')
        setting['login'] = data[1].replace('login:', '').replace(' ', '')
        setting['password'] = data[2].replace('password:', '').replace(' ', '')
        return setting


def Check(data):
    for i in range(0,len(data)-1):
        new1=data[i].replace('https','http')
        for j in range(i+1,len(data)):
            try:
                new2=data[j].replace('https','http')
                if new1==new2:
                    data.remove(data[i])
            except:
                return data
    return data
setting = Settings()
conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
            'host'] + '\'' + ' password=\'' + setting['password'] + '\''
try:
            conn = psycopg2.connect(conn_string)
except psycopg2.Error as err:
            print("Connection error: {}".format(err))




sql='''SELECT symbol,twitter FROM coinmarketcap.coin_links where twitter is not null;'''
try:
    cur = conn.cursor()

    cur.execute(sql)
    data=cur.fetchall()
except psycopg2.Error as err:
    print("Query error: {}".format(err))

for d in data:
    temp_d=d[1].split(';')
    temp_d=Check(temp_d)
    try:
        temp_d.remove('')
    except:
        d=0
    for t in temp_d:

        http = urllib3.PoolManager()
        try:
            r = http.request('GET', t)
        except:
            continue
        soup = BeautifulSoup(r.data)
        ul=soup.find('ul',{'class':'ProfileNav-list'})
        try:
            li=ul.find_all('li')
        except:
            continue
        tw={}
        tw['followers'] = '0'
        tw['twitter'] = '0'
        tw['following'] = '0'
        tw['likes'] = '0'
        for l in li:

            text=l.text
            text=text.split('\n')
            search=[i for i in text if i=='Twetts' or i=='Твиты']
            if len(search)>0:

                l=l.find('span',{'class':'ProfileNav-value'})
                tw['twitter']=l.attrs['data-count']



                continue
            search=[i for i in text if i=='Following' or i=='Читаемые']
            if len(search)>0:
                l = l.find('span', {'class': 'ProfileNav-value'})
                tw['following']=l.attrs['data-count']
                continue
            search = [i for i in text if i == 'Followers' or i == 'Читатели']
            if len(search) > 0:
                l = l.find('span', {'class': 'ProfileNav-value'})
                tw['followers'] = l.attrs['data-count']

                continue
            search = [i for i in text if i == 'Likes' or i == 'Нравится']
            if len(search) > 0:
                l = l.find('span', {'class': 'ProfileNav-value'})
                tw['likes'] = l.attrs['data-count']
                break

        sql='''INSERT INTO twitter.coin (symbol, link, twitts, followers, "following", likes, processing_dttm) VALUES('''
        sql+='\''+d[0]+'\','+'\''+t+'\','+tw['twitter']+','+tw['followers']+','+tw['following']+','+tw['likes']+',\''+str(datetime.datetime.now())+'\');'
        try:
            cur = conn.cursor()

            cur.execute(sql)
            conn.commit()

        except psycopg2.Error as err:
            print("Query error: {}".format(err))
