import urllib3
from bs4 import BeautifulSoup

import re
import psycopg2
from datetime import  datetime
def Settings():
    file = '/www/telegram/settings.txt'
    with open(file, 'r', encoding='utf8') as f:
        data = f.read()
        data = data.split('\n')
        setting = {}
        setting['host'] = data[0].replace('host:', '').replace(' ', '')
        setting['login'] = data[1].replace('login:', '').replace(' ', '')
        setting['password'] = data[2].replace('password:', '').replace(' ', '')
        return setting

http = urllib3.PoolManager()
setting = Settings()
conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
            'host'] + '\'' + ' password=\'' + setting['password'] + '\''
try:
            conn = psycopg2.connect(conn_string)
except psycopg2.Error as err:
            print("Connection error: {}".format(err))


sql='''SELECT symbol,medium,website FROM coinmarketcap.coin_links;'''
try:
    cur = conn.cursor()

    cur.execute(sql)
    data = cur.fetchall()

except psycopg2.Error as err:
    print("Query error: {}".format(err))

for url in data:
    if url[1] is not  None:
        try:
            r = http.request('GET', url[1])
        except:
            continue
        soup = BeautifulSoup(r.data)
        img = soup.find_all('a', {'class': 'ag'})
        for i in img:
            d=i.text
            if re.search('Followers',d):
                fol=d.replace(' Followers','')
                if re.search('K',fol):
                    fol=float(fol.replace('K',''))*1000
                sql='''INSERT INTO medium.coin (symbol, processing_dttm, website, followers) VALUES('''
                sql+='\''+url[0]+'\','+'\''+str(datetime.now())+'\','+'\''+url[1]+'\','+str(fol)+');'
                try:
                    cur = conn.cursor()

                    cur.execute(sql)
                    conn.commit()

                except psycopg2.Error as err:
                    print("Query error: {}".format(err))
                break