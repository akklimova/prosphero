from telethon import TelegramClient
import sys
import re

from datetime import datetime, date, time
import psycopg2

from telethon.tl.functions.messages import GetHistoryRequest
from telethon.utils import InputPeerChannel
from telethon.tl.functions.contacts import ResolveUsernameRequest


def GetLastDate(chat):
    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))
    sql = """SELECT "date" FROM telegram.message where name_chat='Crypto' order by "date" limit 1;"""
    sql = sql.replace('Crypto', chat)

    try:
        cur = conn.cursor()

        cur.execute(sql)

        data = cur.fetchall()
    except psycopg2.Error as err:
        print("Query error: {}".format(err))
    try:
        date = data[0][0]
    except:
        date = -1
    return date

def Read_new_Chat():
    client=Client_start()
    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))
    with open('/www/telegram/New_Chat.txt','r',encoding='utf8') as f:
        new_chat=f.read()

    f.close()
    new_chat=new_chat.split('\n')
    if len(new_chat)==0:
        print('Not found record\n')
        return -1
    Add=[]
    for chat in new_chat:
        if chat != '':
         if re.search('Insert',chat)==None:
                chat_l=chat.replace('\t','').replace('\n','')

                lonami = client.get_entity('CryptoPortfolioChat')
                chat_l=lonami.username
                sql="""SELECT id_message FROM telegram.message where name_chat='CryptoCharity';"""
                sql=sql.replace('CryptoCharity',chat_l)

                cur = conn.cursor()

                cur.execute(sql)
                data=cur.fetchall()
                if len(data)>0:
                  Add.append('chat {} exist'.format(chat_l))
                  new_chat.remove(chat)
                  sql = """SELECT "name" FROM telegram."source" WHERE address='Bitbns';"""
                  sql=sql.replace('Bitbns',chat_l)
                  cur = conn.cursor()

                  cur.execute(sql)
                  data = cur.fetchall()
                  if len(data)==0:
                      if lonami.megagroup == True:
                          type = 'chat'
                      else:
                          type = 'channel'
                      name = lonami.title
                      sql = """INSERT INTO telegram."source"("name", address, "type", id, processed_dttm)VALUES("""
                      sql = sql + '\'' + name + '\',' + '\'' + lonami.username + '\',' + '\'' + type + '\',' + str(
                          lonami.id) + ',\'' + str(datetime.now()) + '\');'
                      try:
                          cur = conn.cursor()
                          # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
                          cur.execute(sql)

                          conn.commit()
                      except psycopg2.Error as err:
                          print("Query error: {}".format(err))
                  with open('/www/telegram/log.txt', 'a', encoding='utf8') as f:
                    f.write('chat - ' + chat + ' already exists '+str(datetime.now())+'\n')
                    f.close()
                else:



                    if lonami!=None:

                            if lonami.megagroup == True:
                                type = 'chat'
                            else:
                                type = 'channel'
                            name=lonami.title

                            if re.search('\'',name):
                                         name=name.replace('\'','"')
                            sql = """INSERT INTO telegram."source"("name", address, "type", id, processed_dttm)VALUES("""
                            sql = sql + '\'' + name+ '\',' + '\'' + lonami.username + '\',' + '\'' + type + '\',' + str(
                                lonami.id) + ',\''+str(datetime.now())+'\');'
                            try:
                                cur = conn.cursor()
                                # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
                                cur.execute(sql)

                                conn.commit()
                            except psycopg2.Error as err:
                                print("Query error: {}".format(err))
                            if LoadtMessage_NewChat(chat, client, conn)==-1:
                                continue
                            with open('/www/telegram/log.txt', 'a', encoding='utf8') as f:
                                    f.write('\n'+chat+' successful write '+str(datetime.now())+'\n')
                                    f.close()
                            with open('/www/telegram/New_Chat.txt', 'a', encoding='utf8') as f:
                                    f.write(chat + ' Insert ' + str(datetime.now())+'\n')
                                    f.close()
                            GetHistoty(chat,client,conn)
                            Add.append('chat {} add'.format(chat))
                            new_chat.remove(chat)
                         ##   Postgres.Delete_Equal(conn, chat)
                       ##     Analasys_History_d(conn, chat)
                         ##   Analasys_History_h(conn, chat)
                            '''
            except psycopg2.Error as err:
                with open('/www/telegram/log.txt', 'a', encoding='utf8') as f:
                    f.write('Error connect base ' + err+'\n')
                    f.close()
            '''
    conn.close()
    with open('/www/telegram/New_Chat.txt','a',encoding='utf8') as f:
        for ch in Add:
            f.write(ch+'\n')
        f.write('Next chat not write\n')
        for ch in new_chat:
            f.write(ch+'\n')
    f.close()

def Get_idMessage(chat):
    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))
    sql = """SELECT  id_message FROM telegram.message where name_chat='Crypto' order by "date" limit 1;"""
    sql = sql.replace('Crypto', chat)

    try:
        cur = conn.cursor()

        cur.execute(sql)

        data = cur.fetchall()
    except psycopg2.Error as err:
        print("Query error: {}".format(err))
    try:
        data = data[0][0]
    except:
        data = -1
    return data


def GetHistoty(chat,client,conn):
    date = GetLastDate(chat)

    max_offset = Get_idMessage(chat)
    if date == -1:
        print('This chat not find')
        return -1



    LoadtMessage_History(chat, date, 0, max_offset, client,conn)


def Insert_data(from_id, id_message, name_chat, message, date, conn):
    if re.search('\'', message):
        message = message.replace("'", '"')
    sql = 'INSERT INTO telegram.message(from_id, id_message, name_chat, message, date)VALUES (';
    query = str(from_id) + ',' + str(
        id_message) + ',' + '\'' + name_chat + '\'' ',' + '\'' + message + '\'' + ',' + '\'' + str(date) + "'"
    sql = sql + query + ');'

    try:
        cur = conn.cursor()
        # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
        cur.execute(sql)
        conn.commit()

    except psycopg2.Error as err:
        print("Query error: {}".format(err))


def GetDate(conn, chat):
    sql = """SELECT date
  FROM telegram.analysis_of_words
  WHERE name_chat='Bitbns'
  ORDER BY data DESC
LIMIT 1;
"""
    sql = sql.replace('Bitbns', chat)
    try:
        cur = conn.cursor()
        # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
        cur.execute(sql)
        ## conn.commit()
        data = cur.fetchall()
    except psycopg2.Error as err:
        print("Query error: {}".format(err))
        data = []

    return data


def GetID(conn, chat):
    sql = """SELECT id_message
  FROM telegram.message
  WHERE name_chat='Bitbns'
  ORDER BY id_message DESC
LIMIT 1;
"""
    sql = sql.replace('Bitbns', chat)
    try:
        cur = conn.cursor()
        # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
        cur.execute(sql)
        ## conn.commit()
        data = cur.fetchall()
    except psycopg2.Error as err:
        print("Query error: {}".format(err))
        data = []
    data = str(data).replace('(', '').replace(')', '').replace(',', '').replace('\'', '').replace('[', '').replace(']',
                                                                                                                   '')
    data = int(data)
    return data


def GetMessage(chat, date_start, date_end):
    list_word = []
    try:
        conn = psycopg2.connect("dbname='postgres' user='postgres'" \
                                " host='localhost' password='253730'")
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))

    sql = """SELECT  message, date
  FROM public.message
  WHERE name_chat='chat'
  AND date>'date_start' AND date<'date_end';"""
    sql = sql.replace('chat', chat).replace('date_start', date_start).replace('date_end', date_end)
    try:
        cur = conn.cursor()
        # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
        cur.execute(sql)
        ## conn.commit()
        data = cur.fetchall()
    except psycopg2.Error as err:
        print("Query error: {}".format(err))
        data = []
    return data


def LoadtMessage_History(name_chat, offset_date, offset_id, max_offset_id, client,conn):

    temp_date = ''
    count = 0
    count1 = 0
    date_end = datetime.strptime('2017-01-01 00:00', "%Y-%m-%d %H:%M")
    offset_id = max_offset_id - 100
    while offset_id >= 0:
        try:
            result = client(
                GetHistoryRequest(name_chat, offset_id=offset_id, offset_date=offset_date, add_offset=0, limit=100,
                                  max_id=max_offset_id, min_id=0, hash=0))

            if result.messages[0].date < date_end:
                print('Base update on {}'.format(str(date_end)))
                break

            for message in result.messages:
                try:
                    if message.message != None:
                        temp_date = str(message.date)
                        temp = message.from_id
                        if temp == None:
                            from_id = -1
                        else:
                            from_id = message.from_id
                        Insert_data(from_id, message.id, name_chat, message.message, message.date, conn)
                        count += 1
                except:
                    count1 += 1
            offset_id -= 100
        except:
            return -1

    print('Base update on chat {}'.format(name_chat))


def LoadtMessage_fromTelegram(name_chat, client):
    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))

    id = GetID(conn, name_chat)

    count = 0
    for message in client.get_messages(name_chat, limit=1000):
        if message.message != None:
            ##  Insert_data(0,0,0,'n','n',28)
            if (message.id <= id):
                conn.close()
                break
            temp = message.from_id
            if temp == None:
                from_id = -1
            else:
                from_id = message.from_id
            Insert_data(from_id, message.id, message.to.username, message.message, message.date, conn)
            count += 1
    print('Write {} message on chat'.format(count))


def LoadtMessage_NewChat(name_chat, client, conn):
    count = 0
    try:
     for message in client.get_messages(name_chat, limit=5000):
        if message.message != None:
            ##  Insert_data(0,0,0,'n','n',28)

            temp = message.from_id
            if temp == None:
                from_id = -1
            else:
                from_id = message.from_id
            Insert_data(from_id, message.id, message.to.username, message.message, message.date, conn)
            count += 1
    except:
        return -1


def Client_start():
    api_id = '257626'
    api_hash = '01e9a909df518e79df93779710accc48'

    client = TelegramClient('session_download', api_id, api_hash)
    client.connect()
    if client.is_user_authorized() == False:
        phone_number = '+79268341417'
        client.send_code_request(phone_number)
        myself = client.sign_in(phone_number, input(int(92368)))
    client.start()
    return client


def GetData_Chat(name_chat):
    client = Client_start()
    lonami = client.get_entity(name_chat)
    sql = """SELECT  id FROM telegram."source" WHERE  address='Bitbns';"""
    sql = sql.replace('Bitbns', name_chat)
    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))
    try:
        cur = conn.cursor()
        # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
        cur.execute(sql)
        ## conn.commit()
        data = cur.fetchall()
    except psycopg2.Error as err:
        print("Query error: {}".format(err))
        return 0
        ## LoadtMessage_NewChat(name_chat, client, conn)
    if len(data) == 0:
        if lonami.megagroup == True:
            type = 'chat'
        else:
            type = 'channel'
        print('Name: {},     id={},    user_name={},    type={}'.format(lonami.title, lonami.id, lonami.username, type))
        answer = input('Add in DB (y/n)')
        if answer == 'y':
            sql = """INSERT INTO telegram."source" ("name", address, "type", id)VALUES("""
            sql = sql + '\'' + lonami.title + '\',' + '\'' + lonami.username + '\',' + '\'' + type + '\'' + str(
                lonami.id) + ');'
            try:
                cur = conn.cursor()
                # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
                cur.execute(sql)
                ## conn.commit()
                conn.commit()
            except psycopg2.Error as err:
                print("Query error: {}".format(err))
                LoadtMessage_NewChat(name_chat, client, conn)
    else:
        lonami = client.get_entity(name_chat)
        print('Chat already exists')
        print('Name: {},     id={},    user_name={}'.format(lonami.title, lonami.id, lonami.username))


def GetHistory_All():
    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))

    sql = """SELECT  address FROM telegram."source";"""
    try:
        cur = conn.cursor()
        # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
        cur.execute(sql)
        ## conn.commit()
        data = cur.fetchall()
    except psycopg2.Error as err:
        print("Query error: {}".format(err))
    for name in data:
        GetHistoty(name[0])

def Load_message():
    client = Client_start()
    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))

    sql = """SELECT address FROM telegram."source";"""
    try:
        cur = conn.cursor()
        # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
        cur.execute(sql)
        ## conn.commit()
        data = cur.fetchall()
    except psycopg2.Error as err:
        print("Query error: {}".format(err))
    if len(data) > 0:
        for d in data:
            print('Start load message on chat {}'.format(d[0]))
            LoadtMessage_fromTelegram(d[0], client)


##LoadtMessage_History('CryptoCharity','2018-04-01',10000,40000)
def Load_NewChat():

            Read_new_Chat()

def Read_new_Chat():
    client=Client_start()
    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))
    with open('/www/telegram/New_Chat.txt','r',encoding='utf8') as f:
        new_chat=f.read()
    new_chat=new_chat.split('\n')

    for chat in new_chat:
        if chat != '':
         if re.search('Insert',chat)==None:
                sql="""SELECT id_message FROM telegram.message where name_chat='CryptoCharity' limit 1;"""
                sql=sql.replace('CryptoCharity',chat)

                cur = conn.cursor()

                cur.execute(sql)
                data=cur.fetchall()
                if len(data)>0:
                  with open('/www/telegram/log.txt', 'a', encoding='utf8') as f:
                    f.write('chat - ' + chat + ' already exists '+str(datetime.now())+'\n')
                    f.close()
                else:

                    try:
                         lonami = client.get_entity(chat)
                    except:
                        with open('/www/telegram/log.txt', 'a', encoding='utf8') as f:
                            f.write('chat - ' + chat + ' error, no exist ' + str(datetime.now()) + '\n')
                            f.close()
                        continue
                    if lonami!=None:

                            if lonami.megagroup == True:
                                type = 'chat'
                            else:
                                type = 'channel'
                            name=lonami.title
                            if re.search('\'',name):
                                         name=name.replace('\'','"')
                            sql = """INSERT INTO telegram."source" ("name", address, "type", id)VALUES("""
                            sql = sql + '\'' + name+ '\',' + '\'' + lonami.username + '\',' + '\'' + type + '\',' + str(
                                lonami.id) + ');'
                            try:
                                cur = conn.cursor()
                                # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
                                cur.execute(sql)

                                conn.commit()
                            except psycopg2.Error as err:
                                print("Query error: {}".format(err))
                                LoadtMessage_NewChat(chat, client, conn)
                            with open('/www/telegram/log.txt', 'a', encoding='utf8') as f:
                                    f.write(chat+'successful write '+str(datetime.now())+'\n')
                                    f.close()
                            with open('/www/telegram/New_Chat.txt', 'a', encoding='utf8') as f:
                                    f.write(chat + 'Insert ' + str(datetime.now())+'\n')
                                    f.close()
                            LoadtMessage_NewChat(chat,client,conn)
                            GetHistoty(chat,client,conn)

def Settings():
    file = '/www/telegram/settings.txt'
    with open(file, 'r', encoding='utf8') as f:
        data = f.read()
        data = data.split('\n')
        setting = {}
        setting['host'] = data[0].replace('host:', '').replace(' ', '')
        setting['login'] = data[1].replace('login:', '').replace(' ', '')
        setting['password'] = data[2].replace('password:', '').replace(' ', '')
        return setting
Load_NewChat()



'''
if __name__=="__main__":
    if sys.argv[1]=='add':
        if sys.argv[2]=='all':
            Load_NewChat()
        else:
          name_chat=sys.argv[2]
          client=Client_start()
          setting = Settings()
          conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
              'host'] + '\'' + ' password=\'' + setting['password'] + '\''
          try:
              conn = psycopg2.connect(conn_string)
          except psycopg2.Error as err:
              print("Connection error: {}".format(err))
          try:
           lonami = client.get_entity(name_chat)

           if lonami != None:

              if lonami.megagroup == True:
                  type = 'chat'
              else:
                  type = 'channel'
              name = lonami.title
              if re.search('\'', name):
                  name = name.replace('\'', '"')
              sql = """INSERT INTO telegram."source" ("name", address, "type", id)VALUES("""
              sql = sql + '\'' + name + '\',' + '\'' + lonami.username + '\',' + '\'' + type + '\',' + str(
                  lonami.id) + ');'
              try:
                  cur = conn.cursor()
                  # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
                  cur.execute(sql)

                  conn.commit()
              except psycopg2.Error as err:
                  print("Query error: {}".format(err))
                  LoadtMessage_NewChat(name_chat, client, conn)
              with open('/www/telegram/log.txt', 'a', encoding='utf8') as f:
                  f.write(name_chat + 'successful write ' + str(datetime.now()) + '\n')
                  f.close()
              with open('/www/telegram/New_Chat.txt', 'a', encoding='utf8') as f:
                  f.write(name_chat + 'Insert ' + str(datetime.now()) + '\n')
                  f.close()
              LoadtMessage_NewChat(name_chat, client, conn)
              GetHistoty(name_chat, client, conn)
          except:
              print('{} chat not exist'.format(sys.argv[2]))
    elif  sys.argv[1]=='history':
        client = Client_start()
        setting = Settings()
        conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
            'host'] + '\'' + ' password=\'' + setting['password'] + '\''
        try:
            conn = psycopg2.connect(conn_string)
        except psycopg2.Error as err:
            print("Connection error: {}".format(err))
        if sys.argv[2]=='all':
            flag=True
            date=sys.argv[3]+' 00:00'
            try:

                date_end = datetime.strptime(date, "%Y-%m-%d %H:%M")

                sql = """SELECT address
                            FROM telegram."source"
                            where processed_dttm>'date_end';"""

                sql = sql.replace('date_end', str(date_end))

            except:
                print('wrong date format, format must be view - Y-m-d H:M\n')
                flag = False
            if flag:




             try:
                cur = conn.cursor()
                # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
                cur.execute(sql)
                ## conn.commit()
                data = cur.fetchall()
             except psycopg2.Error as err:
                print("Query error: {}".format(err))
             for d in data:
                print('Start load history chat {}'.format(d[0]))
                GetHistoty(d[0], client, conn)
        else:
            print('Start load history chat {}'.format(sys.argv[2]))
            GetHistoty(sys.argv[2], client, conn)
        conn.close()
    elif sys.argv[1]=='load':
        name_chat=sys.argv[2]

        if name_chat=='all':
            Load_message()
        else:
            client=Client_start()
            LoadtMessage_fromTelegram(name_chat,client)
    elif sys.argv[1]=='help':
        print('\'add\'  name_chat   - Add chat in base\n if ' + '\' add all\' - add new chat in file New_chat.txt')
        print('\'history\' - name_chat  - Load message hitory on chat')
        print('\'history all\' - load history all chat late date. Format - \'history all date(%Y-%m-%d %h:%m\'\n')
        print('\'load\' name_chat   - add new message\n')
        print('\'load all\'    - add new message for all chat\n')


    else:
        print('\'add\'  name_chat   - Add chat in base, if '+'\'new all\' - add new chat in file New_chat.txt')
        print('\'history\' - name_chat  - Load message hitory on chat')
        print('\'history all\' - load history all chat late date. Format - \'history all date(%Y-%m-%d %h:%m\'\n')
        print('\'load\' name_chat   - add new message\n')
        print('\'load all\'    - add new message for all chat\n')






LoadtMessage_NewChat()
client=Client_start()
try:
    conn = psycopg2.connect("dbname='prosphero' user='externalAnna'" \
                            " host='78.108.92.133' password='44iAipyAjHHxkwHgyAPrqPSR5'")
except psycopg2.Error as err:
    print("Connection error: {}".format(err))

sql="""SELECT address
FROM telegram."source"
where processed_dttm>'2018-04-10 05:00';"""

try:
    cur = conn.cursor()
    # cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) # by column name
    cur.execute(sql)
    ## conn.commit()
    data = cur.fetchall()
except psycopg2.Error as err:
    print("Query error: {}".format(err))
for d in data:
    print('Start load history chat {}'.format(d[0]))
    GetHistoty(d[0],client,conn)
'''