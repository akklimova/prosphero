from bs4 import BeautifulSoup
import urllib3
import re
import psycopg2



def Settings():
    file = '/www/telegram/settings.txt'
    with open(file, 'r', encoding='utf8') as f:
        data = f.read()
        data = data.split('\n')
        setting = {}
        setting['host'] = data[0].replace('host:', '').replace(' ', '')
        setting['login'] = data[1].replace('login:', '').replace(' ', '')
        setting['password'] = data[2].replace('password:', '').replace(' ', '')
        return setting



def Get_Links():
    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
            'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
            conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
            print("Connection error: {}".format(err))


    sql='''SELECT "name",website FROM coinmarketcap.coin_links;'''
    try:
         cur = conn.cursor()

         cur.execute(sql)
         data = cur.fetchall()

    except psycopg2.Error as err:
         print("Query error: {}".format(err))
    Contact=[]
    for coin in data:
        Social={}
        Social['facebook']=[]
        Social['twitter']=[]
        Social['github']=[]
        Social['medium']=[]
        Social['blog'] = []
        Social['name']=coin[0]
        Social['google']=[]
        http = urllib3.PoolManager()
        try:
            r = http.request('GET', coin[1])
        except:
            continue
        soup = BeautifulSoup(r.data)
        img=soup.find_all('a')
        for i in img:
            try:
                if re.search('facebook.com',i.attrs['href']):
                    if i.attrs['href'] not in Social['facebook']:
                        Social['facebook'].append(i.attrs['href'])
                elif re.search('twitter.com',i.attrs['href']):
                    if i.attrs['href'] not in Social['twitter']:
                        Social['twitter'].append(i.attrs['href'])
                elif re.search('gitter.im',i.attrs['href']):
                    Social['gitter']=i.attrs['href']
                    if Social['gitter'][0]=='/':
                        Social['gitter']=coin[1]+Social['gitter']
                        sql = '''UPDATE coinmarketcap.coin_links SET gitter='test2' where "name"='test1';'''
                        sql = sql.replace('test1', coin[0]).replace('test2',Social['gitter'])
                        try:
                            cur = conn.cursor()

                            cur.execute(sql)
                            conn.commit()
                        except psycopg2.Error as err:
                            print("Query error: {}".format(err))
                elif re.search('blog',i.attrs['href']):

                    if Social['blog'][0]=='/':
                        Social['blog']=coin[1]+Social['blog']
                        flag=False
                        for b in Social['blog']:
                            if re.search(b,i.attrs['href']) or re.search(i.attrs['href'],b):
                                flag=True
                                break
                        if flag==False:

                            Social['blog'].append(i.attrs['href'])

                elif re.search('reddit.com',i.attrs['href']):
                    Social['reddit']=i.attrs['href']

                    if Social['reddit'][0] == '/':
                        Social['reddit'] = coin[1] + Social['reddit']
                    sql = '''UPDATE coinmarketcap.coin_links SET reddit='test2' where "name"='test1';'''
                    sql = sql.replace('test1', coin[0]).replace('test2', Social['reddit'])
                    try:
                        cur = conn.cursor()

                        cur.execute(sql)
                        conn.commit()

                    except psycopg2.Error as err:
                        print("Query error: {}".format(err))

                elif re.search('youtube.com',i.attrs['href']):
                    Social['youtube']=i.attrs['href']

                    Social['youtube'] = i.attrs['href']
                    if Social['youtube'][0] == '/':
                        Social['youtube'] = coin[1] + Social['youtube']
                    sql = '''UPDATE coinmarketcap.coin_links SET youtube='test2' where "name"='test1';'''
                    sql = sql.replace('test1', coin[0]).replace('test2', Social['youtube'])
                    try:
                        cur = conn.cursor()

                        cur.execute(sql)
                        conn.commit()

                    except psycopg2.Error as err:
                        print("Query error: {}".format(err))
                elif re.search('medium.com',i.attrs['href']):


                    Social['medium'].append(i.attrs['href'])



                elif re.search('github.com',i.attrs['href']):

                    count=[i for i in i.attrs['href'] if i=='/']
                    if len(count)>4:
                        continue
                    if i.attrs['href'] not in  Social['github']:
                        Social['github'].append(i.attrs['href'])

                elif re.search('Linkedin.com', i.attrs['href']):
                    Social['Linkedin'] = i.attrs['href']
                    Social['Linkedin'] = i.attrs['href']
                    if Social['Linkedin'][0] == '/':
                        Social['Linkedin'] = coin[1] + Social['github']
                    sql = '''UPDATE coinmarketcap.coin_links SET linkedin='test2' where "name"='test1';'''
                    sql = sql.replace('test1', coin[0]).replace('test2', Social['Linkedin'])
                    try:
                        cur = conn.cursor()

                        cur.execute(sql)
                        conn.commit()

                    except psycopg2.Error as err:
                        print("Query error: {}".format(err))
                elif re.search('plus.google.com',i.attrs['href']):
                    if i.attrs['href'] not in Social['google']:

                        Social['google']=i.attrs['href']
                        Social['google'] = i.attrs['href']
                        if Social['google'][0] == '/':
                            Social['google'] = coin[1] + Social['google']
                        sql = '''UPDATE coinmarketcap.coin_links SET google='test2' where "name"='test1';'''
                        sql = sql.replace('test1', coin[0]).replace('test2', Social['google'])
                        try:
                            cur = conn.cursor()

                            cur.execute(sql)
                            conn.commit()

                        except psycopg2.Error as err:
                            print("Query error: {}".format(err))

            except:
                continue

        print(Social)

        if len(Social['facebook'])>0:
            facebook=''
            for f in Social['facebook']:
                facebook+=f+';'
            sql='''UPDATE coinmarketcap.coin_links SET facebook='test2' where "name"='test1';'''
            sql=sql.replace('test1',coin[0]).replace('test2',facebook)
            try:
                cur = conn.cursor()

                cur.execute(sql)
                conn.commit()

            except psycopg2.Error as err:
                print("Query error: {}".format(err))
        if len(Social['blog'])>0:
            blog=''
            for f in Social['blog']:
                blog+=f+';'
            sql='''UPDATE coinmarketcap.coin_links SET blog='test2' where "name"='test1';'''
            sql=sql.replace('test1',coin[0]).replace('test2',blog)
            try:
                cur = conn.cursor()

                cur.execute(sql)
                conn.commit()

            except psycopg2.Error as err:
                print("Query error: {}".format(err))
        if len(Social['twitter']) > 0:
            twitter = ''
            for f in Social['twitter']:
                twitter += f + ';'
            sql = '''UPDATE coinmarketcap.coin_links SET twitter='test2' where "name"='test1';'''
            sql = sql.replace('test1', coin[0]).replace('test2', twitter)
            try:
                cur = conn.cursor()

                cur.execute(sql)
                conn.commit()

            except psycopg2.Error as err:
                print("Query error: {}".format(err))
        if len(Social['github']) > 0:
            github = ''
            for f in Social['github']:
                github += f + ';'
            sql = '''UPDATE coinmarketcap.coin_links SET github='test2' where "name"='test1';'''
            sql = sql.replace('test1', coin[0]).replace('test2', github)
            try:
                cur = conn.cursor()

                cur.execute(sql)
                conn.commit()

            except psycopg2.Error as err:
                print("Query error: {}".format(err))
        if len(Social['medium']):
            min=len(Social['medium'][0])
            index=0
            for i in range(1,len(Social['medium'])):
                if min>len(Social['medium'][i]):
                    index=i
                    min=len(Social['medium'][i])
            sql = '''UPDATE coinmarketcap.coin_links SET medium='test2' where "name"='test1';'''
            sql = sql.replace('test1', coin[0]).replace('test2', Social['medium'][index])
            try:
                cur = conn.cursor()

                cur.execute(sql)
                conn.commit()

            except psycopg2.Error as err:
                print("Query error: {}".format(err))





def Clear_error():
    setting = Settings()
    conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
        'host'] + '\'' + ' password=\'' + setting['password'] + '\''
    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.Error as err:
        print("Connection error: {}".format(err))
    sql='''SELECT "name", internal, website, facebook, twitter, linkedin, telegram, google, vkontakte, blog, youtube, github, reddit, gitter, meetups, stack_excahge, chat FROM coinmarketcap.coin_links;'''

    try:
        cur = conn.cursor()

        cur.execute(sql)
        data=cur.fetchall()

    except psycopg2.Error as err:
        print("Query error: {}".format(err))
    for d in data:
        if d[3] is not None:

         if re.search('facebook',d[3])==None:
            sql='''UPDATE coinmarketcap.coin_links SET facebook='' where "name"='test1';'''
            sql=sql.replace('test1',d[0])
            try:
                cur = conn.cursor()

                cur.execute(sql)
                conn.commit()

            except psycopg2.Error as err:
                print("Query error: {}".format(err))
        if d[9] is not None:
         if re.search('blog',d[9])==None:
            sql='''UPDATE coinmarketcap.coin_links SET blog='' where "name"='test1';'''
            sql=sql.replace('test1',d[0])
            try:
                cur = conn.cursor()

                cur.execute(sql)
                conn.commit()

            except psycopg2.Error as err:
                print("Query error: {}".format(err))


def GetSymbol():
        setting = Settings()
        conn_string = "dbname='prosphero' user=\'" + setting['login'] + '\'' + " host=\'" + setting[
            'host'] + '\'' + ' password=\'' + setting['password'] + '\''
        try:
            conn = psycopg2.connect(conn_string)
        except psycopg2.Error as err:
            print("Connection error: {}".format(err))
        sql = '''SELECT "name",symbol, internal, website, facebook, twitter, linkedin, telegram, google, vkontakte, blog, youtube, github, reddit, gitter, meetups, stack_excahge, chat FROM coinmarketcap.coin_links;'''

        try:
            cur = conn.cursor()

            cur.execute(sql)
            data = cur.fetchall()

        except psycopg2.Error as err:
            print("Query error: {}".format(err))
        for d in data:
            http = urllib3.PoolManager()
            try:
                r = http.request('GET', d[2])
            except:
                continue
            soup = BeautifulSoup(r.data)
            img = soup.find_all('span', {"class": "text-bold"})
            symbol=img[0].text.replace('(','').replace(')','')
            sql = '''UPDATE coinmarketcap.coin_links SET symbol='ttt' where "name"='test1' '''
            sql=sql.replace('ttt',symbol).replace('test1',d[0])
            try:
                cur = conn.cursor()

                cur.execute(sql)
                conn.commit()

            except psycopg2.Error as err:
                print("Query error: {}".format(err))



Get_Links()